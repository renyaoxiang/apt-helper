export TOOL_NAME:=apt-helper
export TOOL_VERSION=1.0.2
export TOOL_BIN_NAME=$(TOOL_NAME)
export TOOL_APT_NAME=$(TOOL_NAME)
export TOOL_APT_TARGET_NAME=$(TOOL_APT_NAME)_$(TOOL_VERSION)_amd64.deb

.PHONY: build clean install uninstall run build-apt install-apt uninstall

help:
	@echo "make [build|build-apt|install-apt|uninstall-apt|clean|help|run]"

clean:
	@rm -rf build

clean-all:
	@make -C packages/apt-helper clean
	@rm -rf build

build:
	@make -C packages/apt-helper build

install:
	@make install-apt

uninstall:
	@make uninstall-apt

build-apt:build
	cp -rf src/main/resources/apt build/
	mkdir -p build/apt/usr/bin
	@cp packages/apt-helper/build/target/$(TOOL_BIN_NAME) build/apt/usr/bin
	mkdir -p build/target
	@dpkg -b build/apt build/target/$(TOOL_APT_TARGET_NAME)

install-apt:build-apt
	@sudo dpkg -i build/target/$(TOOL_APT_TARGET_NAME)

uninstall-apt:
	@sudo dpkg -r $(TOOL_APT_NAME)
